
package testvalidacion;

import Validacion.ValidadorCampos;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import static Validacion.TestCalculadora.*;

/**
 *
 * @authors David, Laura & Albert
 */
public class TestValidacion {

    //We instance the class ValidadorCampos
    ValidadorCampos myValidador = new ValidadorCampos();
    public TestValidacion() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    //We test that a certain field is not empty
    @Test
    public void testEmptyField() {
        System.out.println("Test empty field");
        String field = "";
        boolean expResult = false;
        boolean result = myValidador.validaCampNoBuit(field);
        assertEquals(expResult, result);
        if(expResult != result) {
            fail("testEmptyField not pass");
        }
    }
    
    //We test that the first letter of a certain field is uppercase
    @Test
    public void testFirstLetterUppercase() {
        System.out.println("Test first letter uppercase");
        String field = "holaquetaladios";
        boolean expResult = false;
        boolean result = myValidador.validaPrimeraLletraMajuscules(field);
        assertEquals(expResult, result);
        if(expResult != result) {
            fail("testFirstLetterUppercase not pass");
        }
    }
    
    //We test that the length of a ceartain field is shorter than a given value
    @Test
    public void testMaxLength() {
        System.out.println("Test max length");
        String field = "holaquetaladios";
        int maxLength = 10;
        boolean expResult = true;
        boolean result = myValidador.validaTamanyMaximCamp(field, maxLength);
        assertEquals(expResult, result);
        if(expResult != result) {
            fail("testMaxLength not pass");
        }
    }
    
    //We test that a certain number is between 2 given values
    @Test
    public void testIntervalNum() {
        System.out.println("Test interval num");
        int num = 500;
        int numMin = 200;
        int numMax = 300;
        boolean expResult = false;
        boolean result = myValidador.validaIntervalNumero(num, numMin, numMax);
        assertEquals(expResult, result);
        if(expResult != result) {
            fail("testIntervalNum not pass");
        }
    }
    
    //We test that a DNI has a valid format (8 numbers + 1 letter)
    @Test
    public void testValidFormatDNI() {
        System.out.println("Test valid format DNI");
        String DNI = "12345678A";
        boolean expResult = true;
        boolean result = myValidador.validaFormatDNI(DNI);
        assertEquals(expResult, result);
        if(expResult != result) {
            fail("testValidFormatDNI not pass");
        }
    }
    
    //We test that the letter of a DNI corresponds to the the %23 of the 8 numbers
    @Test
    public void testValidLetterDNI() {
        System.out.println("Test valid letter DNI");
        String DNI = "23T";
        boolean expResult = true;
        boolean result = myValidador.validaLletraDNI(DNI);
        assertEquals(expResult, result);
        if(expResult != result) {
            fail("testValidLetterDNI not pass");
        }
    }
    
    //We test that the complete DNI is valid (the previous 2 methods at once)
    @Test
    public void testValidDNI() {
        System.out.println("Test valid DNI");
        String DNI = "71941487V";
        boolean expResult = true;
        boolean result = myValidador.validaDNI(DNI);
        assertEquals(expResult, result);
        if(expResult != result) {
            fail("testValidDNI not pass");
        }
    }
    
    //We are using static class on the Calculator class, so we are calling directly the functions
    //We test the addition of 2 given numbers
    @Test
    public void testAddition() {
        System.out.println("Test addition");
        double num1 = 10;
        double num2 = 5;
        double expResult = 15;
        double result = addition(num1,num2);
        //assertEquals in double need an epsilon value because the double is not an exact value
        assertEquals(expResult, result, 0.00000001);
        if(expResult != result) {
            fail("testAddition not pass");
        }
    }
    
    //We test the subtraction of 2 given numbers
    @Test
    public void testSubtraction() {
        System.out.println("Test subtraction");
        double num1 = 10;
        double num2 = 5;
        double expResult = 5;
        double result = subtraction(num1,num2);
        assertEquals(expResult, result, 0.00000001);
        if(expResult != result) {
            fail("testSubtraction not pass");
        }
    }
    
    //We test the multiplication of 2 given numbers
    @Test
    public void TestMult() {
        System.out.println("Test mult");
        double num1 = 10;
        double num2 = 5;
        double expResult = 50;
        double result = mult(num1,num2);
        assertEquals(expResult, result, 0.00000001);
        if(expResult != result) {
            fail("testMult not pass");
        }
    }
    
    //We test the division of 2 given numbers
    @Test
    public void TestDiv() {
        System.out.println("Test div");
        double num1 = 10;
        double num2 = 5;
        double expResult = 2;
        double result = div(num1,num2);
        assertEquals(expResult, result, 0.00000001);
        if(expResult != result) {
            fail("testDiv not pass");
        }
    }
    
    //We test the mod number of a given number
    @Test
    public void TestMod() {
        System.out.println("Test mod");
        double num1 = 10;
        double num2 = 5;
        double expResult = 0;
        double result = mod(num1,num2);
        assertEquals(expResult, result, 0.00000001);
        if(expResult != result) {
            fail("testMod not pass");

        }
    }
}
