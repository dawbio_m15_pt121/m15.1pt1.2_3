package Validacion;

/**
 * Crea una classe Validador que servirà per verificar camps de formularis; i
 * una classe Main que provi tots els mètodes. Aquests seran: -
 * validaCampNoBuit(String camp) - validaTamanyMaximCamp(String camp, int
 * tamanyMaxCamp) - validaCampEmail(String camp) - validaIntervalNumero(int
 * intMinim, int intMaxim)
 */
public class ValidadorCampos {

    public ValidadorCampos() {

    }

    public void print() {
        System.out.println("hola!");
    }

    //This method checks if a field is empty
    public boolean validaCampNoBuit(String camp) {
        return camp != null && !camp.equals(""); // Otras opciones, camp.equals("");
    }

    //This method checks if the first letter of a field is uppercase
    public boolean validaPrimeraLletraMajuscules(String camp) {
        // String primeraLletra = camp.substring(0,1);
        // String primeraLletraMajuscula = camp.substring(0,1).toUpperCase();
        // return primeraLletra.equals(primeraLletraMajuscula); // Otras opciones, camp.equals("");
        String regex = "^[A-Z][a-z]+$";
        return camp.matches(regex);
    }

    //This method checks if the length of a field exceeds a given size
    public boolean validaTamanyMaximCamp(String camp, int tamanyMaxCamp) {
        return camp.length() > tamanyMaxCamp;
    }

    //This method checks if the email has a valid format
    public boolean validaCampEmail(String email) {
        // https://www.tutorialspoint.com/validate-email-address-in-java 
        String regex = "^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$";
        return email.matches(regex);
    }

    //This method checks if the URL has a valid format
    public boolean validaCampURL(String email) {
        // https://www.tutorialspoint.com/validate-email-address-in-java 
        String regex = "(http://|https://)(www.)?([a-zA-Z0-9]+).[a-zA-Z0-9]*.[a-z]{3}.?([a-z]+)?";
        return email.matches(regex);
    }

    //This method checks if a number is between 2 given numbers
    public boolean validaIntervalNumero(int num, int numMin, int numMax) {
        return numMin < num && numMax > num;
    }

    //This method checks if the DNI has a valid format (8 numbers + 1 letter)
    public boolean validaFormatDNI(String DNI) {
        String regex = "^[0-9]{8}[A-Za-z]$";
        return DNI.matches(regex);
    }

    //This method checks if the letter of the DNI corresponds to the %23 of the 8 numbers
    public boolean validaLletraDNI(String DNI) {
        String letters = "TRWAGMYFPDXBNJZSQVHLCKE";
        String regex = "[A-Z]";
        String number_str = DNI.split(regex)[0];
        int number = Integer.parseInt(number_str);
        char letter_input = DNI.charAt(number_str.length());
        char letter_calc = letters.charAt(number % 23);
        return letter_input == letter_calc;
    }
    
    //This method checks if the complete DNI is valid (2 previous methods at once)
    public boolean validaDNI(String DNI){
        return validaFormatDNI(DNI) && validaLletraDNI(DNI);
    }
}
