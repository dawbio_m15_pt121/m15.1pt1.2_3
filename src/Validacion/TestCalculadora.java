package Validacion;

import java.util.Scanner;

public class TestCalculadora {

    public static void main(String[] args) {

    	double num1, num2;
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter the first number:");

        num1 = scanner.nextDouble();
        System.out.print("Enter the second number:");
        num2 = scanner.nextDouble();

        System.out.print("Enter an operator (+, -, *, /, %): ");
        char operator = scanner.next().charAt(0);

        scanner.close();

        switch(operator){
            case '+':
            	addition(num1,num2);
                break;

            case '-':
            	subtraction(num1,num2);
                break;

            case '*':
            	mult(num1,num2);
                break;

            case '/':
            	div(num1,num2);
                break;
            case '%':
                mod(num1,num2);
                break;
            default:
                System.out.printf("You have entered a wrong operator");
                break;
        }
    }
    
    //This method calculates the addition of 2 given numbers
    public static double addition(double num1, double num2){
        double output = num1 + num2;
        return output;
    }
    
    //This method calculates the subtraction of 2 given numbers
    public static double subtraction(double num1, double num2){
        double output = num1 - num2;
        return output;
    }
    
    //This method calculates the multiplication of 2 given numbers
    public static double mult(double num1, double num2){
        double output = num1 * num2;
        return output;
    }
    
    //This method calculates the division of 2 given numbers
    public static double div(double num1, double num2){
        double output = num1 / num2;
        return output;
    }
    
    //This method calculates the mod number of a given number
    public static double mod(double num1, double num2){
        double output = num1 % num2;
        return output;
    }
}